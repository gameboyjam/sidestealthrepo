﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {
	public bool autoLoadNextLevel;
	public float timeToAutoLoad;
	// Use this for initialization
	void Start () {
		if (autoLoadNextLevel) {
			Invoke ("LoadNextLevel", timeToAutoLoad);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}




	public void LoadNextLevel(){
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
	
	}
	public  void LoadLevel(string level){

		SceneManager.LoadScene (level);

	}
	public void LoadLevelInt(int level){
		SceneManager.LoadScene (level);

	}
	public void Quit(){
		Application.Quit ();
	}
}
