﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class SelectionIndicator : MonoBehaviour {
	private List<string> selectionStates = new List<string> {"top","down","middle"};
	public Transform[] positions;
	public string actualSelection="top";
	private int actualSelectionNumber;
	private LevelManager levelManager;
	public AudioClip switchSound;
	// Use this for initialization
	void Start () {
		levelManager = FindObjectOfType<LevelManager> ();
		gameObject.AddComponent<AudioSource>();
		switchSound = Resources.Load ("switchSound") as AudioClip;
	}
	
	// Update is called once per frame
	void Update () {
		actualSelection = selectionStates [actualSelectionNumber];

		if (actualSelection == "top") {//this is to play
			transform.position = positions [0].position;
			if (Input.GetKey (KeyCode.J)) {
				levelManager.LoadNextLevel ();
			}
		}
		if (actualSelection == "middle") {//quit
			transform.position = positions [1].position;
			if (Input.GetKey (KeyCode.J)) {
				levelManager.Quit ();
			}
		}
		if (actualSelection == "down") {//credits
			transform.position = positions [2].position;
			if (Input.GetKey (KeyCode.J)) {
				levelManager.LoadLevel ("Credits");//gotta name the scene like this
			}
		}


		if (Input.anyKey) {
			SwitchSelections ();

		}

	}

	void SwitchSelections(){
		

		if (Input.GetKeyDown (KeyCode.UpArrow)) {
			GetComponent<AudioSource> ().PlayOneShot (switchSound);
			actualSelectionNumber=(actualSelectionNumber+1)%3;
			print (actualSelectionNumber);

		}
		if (Input.GetKeyDown (KeyCode.DownArrow)) {
			GetComponent<AudioSource> ().PlayOneShot (switchSound);
			actualSelectionNumber=(actualSelectionNumber-1)%3;
			if (actualSelectionNumber == -1) {
				actualSelectionNumber = 2;
			}
			print (actualSelectionNumber);

		}
	

	}
	void ChooseLevel(){
	
	
	
	}
}
