﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour {
	public int health = 2;
	private SpriteRenderer spriteRenderer;
	public Sprite deadSprite;
	// Use this for initialization
	void Awake () {
		spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.GetComponent<Bullet> ()) {
			if (!other.GetComponent<Bullet> ().isEnemyBullet) {
				print ("enemy hit by a bullet coming from the player");
				health--;
                other.GetComponent<SpriteRenderer> ().enabled = false;
                
			}
		
			if (health <= 0) {
				print ("EnemyKilled");
                //Destroy (gameObject);
                StartCoroutine(killSelf());


            }

		}
	}
	private IEnumerator killSelf ()
	{
		//Let's get rid of the flashlight gameobject, because then he'd be wasting the flashlight's batteries.
		Destroy(transform.GetChild(0).gameObject);
		//Let's disable the physics as well, so our player can feel like a boss and walk all over him.
		gameObject.GetComponent<BoxCollider2D>().enabled = false;
		gameObject.GetComponent<EnemyScript>().enabled = false;
		//Now, let's show dat fancy sprite!
		spriteRenderer.sprite = deadSprite;
		yield return new WaitForSeconds(20);
		//Just kill him already!
		Destroy(transform.parent.gameObject);
	}
}
