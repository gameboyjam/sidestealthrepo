﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class PlayearHealth : MonoBehaviour {
	public int health;
	public Image healthBar; 
	public Sprite playerDeath;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		healthBar.fillAmount = health / 50f;
			//if the health is 50 the fill amount should be 1
			//if the health is 0 the fill amount is 0
			//so the fill amount it health/50

	}
	/*void OnTriggerEnter2D(Collider2D other){
		if (other.GetComponent<Bullet> ()) {
			if (other.GetComponent<Bullet> ().isEnemyBullet) {
				health--;
				print ("playerGettingShotAt");
				other.gameObject.GetComponent<SpriteRenderer>().enabled=false;
			}}

		if (health <= 0) {
			print("playerGotKilled!");
			GetComponent<CharacterMovement> ().enabled = false;
			GetComponent<Animator> ().enabled = false;
			FindObjectOfType<GameManager> ().StopMovement ();
			GetComponent<SpriteRenderer> ().sprite = playerDeath;
		}
	}*/

		

	public void KillPlayer(){
		GetComponent<CharacterMovement> ().enabled = false;
		GetComponent<Animator> ().enabled = false;
		FindObjectOfType<GameManager> ().StopMovement ();
		GetComponent<SpriteRenderer> ().sprite = playerDeath;
        //Let's store the distance.
        float meters = FindObjectOfType<FloorScrolling>().getMeters();
        ScoreManager.Distance = Mathf.RoundToInt(meters);
        FindObjectOfType<LevelManager>().LoadLevel("PlayerCaptured");

        StartCoroutine (LoadDeathLevelIn (3));
	
	}


	IEnumerator LoadDeathLevelIn(int seconds){
		yield return new WaitForSeconds (seconds);
		FindObjectOfType<LevelManager> ().LoadLevel ("PlayerCaptured");
	}







}
