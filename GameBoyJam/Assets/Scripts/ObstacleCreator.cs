﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


/// <summary>
/// This class will spwan randomly sprites
/// in different position of y and at random times
/// </summary>
///


public class ObstacleCreator : MonoBehaviour {


    public List<GameObject> spritesToSpaw;

    // stores the number of partitions of the grid to spawn the sprites
    public int gridPartition;
    //Stablish how much time it's needed to wait until next spawn
    public int maxNumberOfObjects = 2;

    private float increment;
    private Vector2 topLeftCorner;
    private Vector2 size;

    private bool canSpawn = true;
    private bool outsideCollider = false;

    private List<bool> lastRow = new List<bool>();
    private List<bool> currentRow = new List<bool>();

    public float delaySeconds = 0.1f;
    private bool forceToStop = false;

    private float timeToSpawnAstar = -6.0f;
    public GameObject Astar;

    public GameObject gridDimension;
    public GameObject spriteContainer;
    public bool followGrid = false;

    private bool obstacleCreated = false;


	// Use this for initialization
	void Start ()
    {
        if(spritesToSpaw == null)
        {
            spritesToSpaw = new List<GameObject>();
        }

        //Get the size of the box collider
        size = GetComponent<BoxCollider2D>().size;
        //calculate the increment between spawing spaces, basicly get the heigh tof the cube and divide by the number of partitions define by gridPartition
        increment = size.y / gridPartition;
        //Store the topLeft corner of the box for spawing position
        topLeftCorner = new Vector2(transform.position.x - size.x *0.5f, transform.position.y + size.y *0.5f);
        resetList();
        //Avoid not having at least one space when create a row
        maxNumberOfObjects = Mathf.Min(maxNumberOfObjects, gridPartition - 1);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(!forceToStop)
        {
            if (canSpawn)
            {
                SpawnRow();
                canSpawn = false;
            }
            if (timeToSpawnAstar > 4)
            {
                Instantiate(Astar, new Vector2(transform.position.x, transform.position.y), Quaternion.identity);
                timeToSpawnAstar = 0;
            }
            timeToSpawnAstar += Time.deltaTime;
        }
        
	}


    void SpawnRow()
    {
        //Spawn the grid Dimension in the middle, this it's just used to define when a new row will be created
        if(followGrid)
        {
            GameObject go = Instantiate(gridDimension, transform.position + new Vector3(0.0f, size.y * 0.5f, 0.0f), Quaternion.identity) as GameObject;
            go.transform.parent = spriteContainer.transform;
        }
        
        //Create some random obstacles
        for(int i = 0; i < maxNumberOfObjects; i++)
        {
            SpawnObstacle();
        }
        createEmptyIfNoRow();
        copyLits(currentRow, lastRow);
        resetCurrentList();
        obstacleCreated = false;
    }

    void createEmptyIfNoRow()
    {
        if(!followGrid && !obstacleCreated)
        {
            GameObject go = Instantiate(gridDimension, transform.position + new Vector3(0.0f, size.y * 0.5f, 0.0f), Quaternion.identity) as GameObject;
            go.transform.parent = spriteContainer.transform;
            resetLastList();
        }
    }
    /// <summary>
    /// This method create a sprite on a position using the spawnGenerator
    /// </summary>
    void SpawnSprite(int position, GameObject sprite)
    {
        //Calculate Y position of the sprite
        float spriteY = topLeftCorner.y - (increment) * position + increment*0.5f;
        GameObject spr = Instantiate(sprite, new Vector3(transform.position.x, spriteY, transform.position.z), sprite.transform.rotation)as GameObject;
        spr.transform.parent = spriteContainer.transform;
    }

    /// <summary>
    /// Spawns an obstacle y a random Y position
    /// </summary>
    void SpawnObstacle()
    {
        //Check last row ro avoid closing paths
        int position = Random.Range(1, gridPartition+1);
        int rowPosition = position - 1;
        bool canCreate = !currentRow[rowPosition];
        
        if (canCreate)
        {
            //Check last line have an obstacle, or it's empty
            if (lastRow[rowPosition] || !lastRow.Contains(true) )
            {
                createObstacle(position);
            }
            else
            {
                bool canUp = false;
                bool canDown = false;
                //Check up/down to see if there is any exit
                if(rowPosition > 0)
                {
                    //Check free up
                    if(!lastRow[rowPosition-1])
                    {
                        canUp = true;
                    }
                }

                if(rowPosition < gridPartition-1)
                {
                    if (!lastRow[rowPosition + 1])
                    {
                        canDown = true;
                    }
                }

                if(canUp || canDown)
                {
                    
                    if(canUp && canDown)
                    {
                        //Choose Random
                        if(Random.value > 0.5f)
                        {
                            //Up
                            canUp = true;
                            canDown = false;
                        }
                        else
                        {
                            canUp = false;
                            canDown = true;
                        }
                    }
                    bool pathCleared = false;
                    if(canUp && !lastRow[rowPosition - 1])
                    {
                        currentRow[rowPosition - 1] = true;
                        pathCleared = true;
                    }
                    else if(!lastRow[rowPosition + 1])
                    {
                        currentRow[rowPosition + 1] = true;
                        pathCleared = true;
                    }

                    if(pathCleared)
                    {
                        createObstacle(position);
                    }
                    
                }
                else
                {
                    //Try to open a path
                    if(rowPosition - 1 > 0 && !currentRow[rowPosition - 1])
                    {
                        currentRow[rowPosition - 1] = true;
                    }
                    
                    if(rowPosition + 1 < gridPartition - 1 && !currentRow[rowPosition + 1])
                    {
                        currentRow[rowPosition + 1] = true;
                    }
                }
            }
          
        }
    }

    void createObstacle(int position)
    {
        int spriteIndex = Random.Range(0, spritesToSpaw.Count);
        GameObject obstacle = spritesToSpaw[spriteIndex];
        SpawnSprite(position, obstacle);
        currentRow[position -1] = true;
        obstacleCreated = true;
    }
    /// <summary>
    /// Draws lines with the paths of object creation
    /// </summary>
    void OnDrawGizmos()
    {
        Vector2 gizmoSize = GetComponent<BoxCollider2D>().size;
        float increment = gizmoSize.y / gridPartition;

        Vector2 topLeftCorner = new Vector2(transform.position.x - gizmoSize.x * 0.5f, transform.position.y + gizmoSize.y * 0.5f);

        float leftX = topLeftCorner.x;
        float rightX = topLeftCorner.x + size.x;
        float currentY = topLeftCorner.y;

        Gizmos.DrawLine(new Vector3(leftX, currentY), new Vector3(rightX, currentY)); 
        for(int i = 0; i < gridPartition; i++)
        {
            float newY = currentY - increment * i;
            Gizmos.DrawLine(new Vector3(leftX, newY), new Vector3(rightX, newY));
        }

    }
    /// <summary>
    /// Sets when a new sprite can be created
    /// </summary>
    /// <param name="coll"></param>
    void OnTriggerExit2D(Collider2D coll)
    {

        if(followGrid)
        {
            if (coll.gameObject.CompareTag("Grid"))
            {
                if (!outsideCollider)
                {
                    outsideCollider = true;
                    Invoke("allowRow", delaySeconds);
                }
            }
        }
        else
        {
            if (!outsideCollider)
            {
                outsideCollider = true;
                Invoke("allowRow", delaySeconds);
            }
        }
       
        
    }

    void resetLastList()
    {
        lastRow.Clear();
       
        for(int i = 0; i < gridPartition; i++)
        {
            lastRow.Add(true);
            
        }
    }

    void resetCurrentList()
    {
        currentRow.Clear();
        for (int i = 0; i < gridPartition; i++)
        {
            currentRow.Add(false);
        }
    }

    void resetList()
    {
        resetLastList();
        resetCurrentList();
    }

    void allowRow()
    {
        canSpawn = true;
        outsideCollider = false;
    }

    public void stop()
    {
        forceToStop = true;
    }

    public void start()
    {
        forceToStop = false;
    }

    public void copyLits(List<bool> origin, List<bool> dest)
    {
        dest.Clear();
        for(int i = 0; i < origin.Count; i++)
        {
            dest.Add(origin[i]);
        }
    }
}
