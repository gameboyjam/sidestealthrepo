﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This class shows the Box2DCollider as a Gizmo
/// </summary>
[RequireComponent(typeof(BoxCollider2D))]
public class GizmoBox : MonoBehaviour {

    public Color c;
    public bool isWire = false;
    /// <summary>
    /// Draws BoxCollider2D as a cube
    /// </summary>
    void OnDrawGizmos()
    {
        Vector3 cubeSize = Vector3.zero;
        cubeSize.x = GetComponent<BoxCollider2D>().size.x;
        cubeSize.y = GetComponent<BoxCollider2D>().size.y;
        //Gizmos.color = c;
        if(isWire)
        {
            Gizmos.DrawWireCube(transform.position, cubeSize);
        }
        else
        {
            Gizmos.DrawCube(transform.position, cubeSize);
        }
        
    }
}
