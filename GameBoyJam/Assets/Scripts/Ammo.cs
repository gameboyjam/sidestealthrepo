﻿using UnityEngine;
using System.Collections;

public class Ammo : MonoBehaviour
{

    public int ammoAmount = 25;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Shooting shootingPl = other.GetComponentInParent<Shooting>();
            shootingPl.AddAmmo(ammoAmount);
            Destroy(gameObject);
        }
    }
}
