﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HighScoreDisplay : MonoBehaviour {
	private Text text;

	void Awake () {
		text = gameObject.GetComponent<Text>();
	}
	void Start()
	{
		if (ScoreManager.RecordDistance == ScoreManager.Distance)
		{
			text.text = "New High Score: " + ScoreManager.RecordDistance;
		}
		else
			text.text = "High Score: " + ScoreManager.RecordDistance + " Your score: " + ScoreManager.Distance;
	}
	// Update is called once per frame
	void Update () {

	}
}
