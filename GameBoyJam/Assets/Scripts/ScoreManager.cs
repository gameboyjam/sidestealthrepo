﻿using UnityEngine;
using System.Collections;
/*
 * This script records the record distance and the last distance.
 */
public static class ScoreManager
{
    private static int distance, recordDistance;

    public static int Distance
    {
        get
        {
            return distance;
        }
        set
        {
            distance = value;
            if (distance > recordDistance)
            {
                recordDistance = distance;
            }
        }
    }
    public static int RecordDistance
    {
        get
        {
            return recordDistance;
        }
    }
}