﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshRenderer))]
public class FloorScrolling : MonoBehaviour {

    public float speed = 0.5f;
    private bool m_canMove = true;

    private MeshRenderer meshRenderer;

    public Vector2 direction;
    public int metersToAdvise = 10;



    public delegate void TenMeters(float m_meters);
    public static event TenMeters onMeters;

    public Texture firstTxt;
    public Texture secondTxt;

    private int txtID = 0;

    private float m_currentMeters;
    private float m_meters = 0.0f;

    // Use this for initialization
    void Start ()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        //Multiply the direction by -1 because it's going in the contrary direction to the world
        direction *= -1;
        m_meters = 0.0f;
        m_currentMeters = 0.0f;
        meshRenderer.material.mainTexture = firstTxt;
	}
	
	// Update is called once per frame
	void Update () {

        if(m_canMove)
        {
            Vector2 offset = direction * speed * Time.deltaTime;
            m_meters += Mathf.Abs(speed * Time.deltaTime) * 100f;
            m_currentMeters += Mathf.Abs(speed * Time.deltaTime) * 100f;
            meshRenderer.material.mainTextureOffset += offset;
            
        }

        if (m_currentMeters > metersToAdvise)
        {
            if(onMeters != null)
            {
                onMeters(m_meters);
            }
            m_currentMeters = 0.0f;
        }
	}

    public void SetMove(bool canMove)
    {
        m_canMove = canMove;
    }

    public float getMeters()
    {
        return m_meters;
    }

    public void swapTextures()
    {
        if(txtID == 0)
        {
            txtID = 1;
            meshRenderer.material.mainTexture = secondTxt;
        }
        else
        {
            txtID = 0;
            meshRenderer.material.mainTexture = firstTxt;
        }
    }
}
