﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Shooting : MonoBehaviour {
	public GameObject bulletGO;//this needs to have a bullet component
	// Use this for initialization
	public GameObject bulletsParent;
	public float bulletFrecuency;//the smaller the faster you shoot
	public CharacterMovement thisCharacterMovement;

	public Animator playerAnim;

	public bool hasEnoughAmmo=true;
	public Image ammoImage;
	public int ammo;
	public bool isAiming=false;
	void Start(){
		playerAnim = GetComponent<Animator> ();

	}


	// Update is called once per frame
	void Update () {
		ammoImage.fillAmount = ammo / 50f;//dont know if this is going to be costly grafically 
		if (Input.GetKeyDown (KeyCode.J))
        {
			thisCharacterMovement.StopCharacter ();
			InvokeRepeating ("ShootOnce", 0, bulletFrecuency);
			//I want to stop the character here
		}

		if (Input.GetKeyUp (KeyCode.J))
        {
			thisCharacterMovement.ResumeMovingCharacter ();
			CancelInvoke ();
		}
	}
    /// <summary>
    /// Method called to create a bullet and shoot it
    /// </summary>
	public void ShootOnce()
	{
		hasEnoughAmmo = (ammo > 0);
		if (hasEnoughAmmo)
        {
            
            Vector2 bulletDirection = new Vector2 (Mathf.RoundToInt (Input.GetAxis ("Horizontal")), (Mathf.RoundToInt (Input.GetAxis ("Vertical"))));
            
			if (bulletDirection == Vector2.zero) {
			bulletDirection = GetComponent<CharacterMovement>().lookingDirection();
			
			}
			//dont wanna shoot without speed
				
			
			GameObject bullet = Instantiate (bulletGO, transform.position, Quaternion.identity) as GameObject;//this gotta be the direction the player is facing

			bullet.GetComponent<Rigidbody2D> ().velocity = bullet.GetComponent<Bullet> ().velocity * bulletDirection;//player -enemy transform position

			ammo = ammo - 2;


			bullet.transform.parent = bulletsParent.transform;

		}
	}

    public void AddAmmo(int ammoAmount)
    {
        ammo += ammoAmount;
    }










}
