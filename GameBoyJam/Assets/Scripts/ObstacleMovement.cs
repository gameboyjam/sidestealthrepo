﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This component moves the obstacles flowwoing an specific speed
/// </summary>
/// 

[RequireComponent(typeof(BoxCollider2D))]
public class ObstacleMovement : MonoBehaviour
{

    public float speed = 0.5f;
    public bool m_canMove = true;

    public Vector3 direction;
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

        if (m_canMove)
        {
            //Vector3 offset = new Vector3(-Time.deltaTime * speed, 0, 0);
            transform.position += direction * speed * Time.deltaTime;
        }
    }

    public void SetMove(bool canMove)
    {
        m_canMove = canMove;
    }
}
