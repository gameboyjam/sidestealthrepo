﻿using UnityEngine;
using System.Collections;

public class ObstacleDestroyer : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        //Destroy everything that enters here
        Destroy(other.gameObject);
    }
}
