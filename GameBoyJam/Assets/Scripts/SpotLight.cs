﻿using UnityEngine;
using System.Collections;

public class SpotLight : MonoBehaviour {


    public delegate void SpotLightReached();
    public static event SpotLightReached onSpotLightReached;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            //Stops all!
            //send singnal of stop
            if(onSpotLightReached != null)
            {
                onSpotLightReached();
            }
        }
    }
}
