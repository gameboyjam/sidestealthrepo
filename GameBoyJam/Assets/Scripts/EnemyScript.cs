﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyScript : MonoBehaviour
{
    public GameObject Astar;
    private Pathfinding pathFinding;
    private Grid grid;
    public Transform target;
    List<Node> destinations = new List<Node>();
    List<List <Node>> pathToDestination = new List<List<Node>>();
    int destinationId = 0;
    int pathToDestinationId = 0;
    bool foundPlayer = false;
    public Collider2D FieldOfView;

	public GameObject playerCharacter;//this will be used to shoot in the right direction
	public GameObject enemyBullet;
	public GameObject bulletParent;
    public Transform spotLight;
    
	private Vector2 shootingDirection;
    private float directionUpdateTime = 0.0f;
    private float shootUpdateTime = 0.0f;
	private bool isEnemyShooting=false;
    private float shootSpeed = 0.6f;
    private float shootingTimeoutTime = 0.0f;
    private SpriteRenderer spriteRenderer;

    public float movementSpeed = 0.02f;
    public float animationSpeed = 0.5f;
    private int currentAnimationIndex = 0;
    private float currentAnimationTime = 0.0f;


    //I'm lazy (This is Zion) so here is how this is working, I'm not going to do animations (if someone wants to, go ahead!):
    //0-2 = up
    //3-5 = down
    //6-8 = left
    //9-11 = right
    //12-14 = upper left
    //15-17 = upper right
    //18-20 = lower left
    //21-23 = lower right
    //24 = dead
    public Sprite[] enemySprites;
    void Awake()
    {
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        pathFinding = Astar.GetComponent<Pathfinding>();
        grid = Astar.GetComponent<Grid>();
		playerCharacter = FindObjectOfType<CharacterMovement> ().gameObject;
		bulletParent = GameObject.Find ("BulletsParent");
        Physics2D.queriesStartInColliders = false;
    }

    //This won't actually run at the very start of the game, because the enemies are spawned a bit of time afterwards.
    void Start()
    {
        Vector2 worldBottomLeft = Astar.transform.position - Vector3.right * grid.gridWorldSize.x / 2 - Vector3.up * grid.gridWorldSize.y / 2;
        bool okToSpawn = false;
        //This sets a random point on the grid for the enemy to spawn and the moves him there.
        while (okToSpawn == false)
        {
            Vector2 gridPosition = new Vector2(Random.Range(0, grid.gridWorldSize.x), Random.Range(0, grid.gridWorldSize.y));
            Vector2 worldPoint = worldBottomLeft + Vector2.right * (gridPosition.x * (grid.nodeRadius*2) + grid.nodeRadius) + Vector2.up * (gridPosition.y * grid.nodeRadius*2 + grid.nodeRadius);
            if (grid.NodeFromWorldPoint(worldPoint).walkable == true)
            {
                transform.position = worldPoint;
                okToSpawn = true;
            }
        }
        
        //Let's generate some pathfinding!
        
        int destinationCount = Random.Range(2, 6); //Should be randomized, we'll make this easy for now.
        for (int i = 0; i < destinationCount; i++)
        {
            bool good = false;
            int id = 0;
            while (good == false)
            {
                if (id > 30)
                {
                    Destroy(gameObject);
                    Destroy(Astar);
                    good = true;
                }
                Vector2 gridPosition = new Vector2(Random.Range(0, grid.gridWorldSize.x), Random.Range(0, grid.gridWorldSize.y));

                Vector2 worldPoint = worldBottomLeft + Vector2.right * (gridPosition.x * (grid.nodeRadius*2) + grid.nodeRadius) + Vector2.up * (gridPosition.y * grid.nodeRadius*2 + grid.nodeRadius);
                Node destination = new Node(true, worldPoint, Mathf.RoundToInt(gridPosition.x), Mathf.RoundToInt(gridPosition.x));
                //Now that we have the node setup, let's make sure our enemie can access it.
                
                //This generates a new path.
                if (i == 0)
                {
                    pathFinding.FindPath(transform.position, destination.worldPosition);
                }
                else
                {
                    pathFinding.FindPath(destinations[i-1].worldPosition, destination.worldPosition);
                }
                
                //If the enemie can't make it to the destination.
                if (grid.path != null && grid.path.Count != 0)
                {
                    //If this is our first node.
                    if (pathToDestination.Count == 0)
                    {
                        destinations.Add(destination);
                        pathToDestination.Add(grid.path);
                        Debug.Log("grid path count: " + grid.path.Count);
                        good = true;
                    }
                    //To make sure it has generated a new path and is not just using previous one.
                    else if (grid.path != pathToDestination[i-1])
                    {

                        destinations.Add(destination);
                        pathToDestination.Add(grid.path);
                        Debug.Log("grid path count: " + grid.path.Count);
                        good = true;
                    }
                }
                id++;

            }
            Debug.Log(i);
            
        }
        //Node startNode = new Node(true, transform.position, grid.NodeFromWorldPoint(transform.position).gridX, Mathf.RoundToInt(gridPosition.x));
        bool isOk = false;
        var index = 0;
        while (isOk == false)
        {
            if (index > 20)
            {
                GameObject.Destroy(gameObject);
                GameObject.Destroy(Astar);
                isOk = true;
            }
            Node startNode = grid.NodeFromWorldPoint(transform.position);
            if (startNode.walkable == true)
            {
                if (grid.path != null && grid.path.Count != 0)
                {
                    pathFinding.FindPath(destinations[destinations.Count - 1].worldPosition, startNode.worldPosition);
                    destinations.Add(startNode);
                    pathToDestination.Add(grid.path);
                    isOk = true;
                }
            }

            index++;
        }
        
    }

    void Update()
    {
        //Updating the animation'
        currentAnimationTime += Time.deltaTime;
        if (currentAnimationTime > animationSpeed)
        {
            currentAnimationTime = 0.0f;
            if (currentAnimationIndex >= 2)
            {
                currentAnimationIndex = 0;
            }
            else
                currentAnimationIndex++;
        }
        Debug.Log("current Animation Index: "+currentAnimationIndex);

        if (!foundPlayer)
        {
            //Check to see if we need to start moving towards the next node.
            if (Vector2.Distance(transform.localPosition, pathToDestination[destinationId][pathToDestinationId].worldPosition) < 0.05f)
            {
                
                if (pathToDestinationId != pathToDestination[destinationId].Count-1)
                {
                    pathToDestinationId++;
                }
                else
                {
                    pathToDestinationId = 0;
                    if (destinationId != pathToDestination.Count-1)
                    {
                        destinationId++;
                    }
                    else
                    {
                        destinationId = 0;
                    }
                }
            }
            
            try
            {
                //Let's move the player towards the next spot on it's path.
                Vector2 movingPosition = Vector2.MoveTowards(transform.localPosition, pathToDestination[destinationId][pathToDestinationId].worldPosition, movementSpeed);

                transform.localPosition = movingPosition;
                //Now let's rotate the spotlight in the direction it is moving.
                Vector3 dir = pathToDestination[destinationId][pathToDestinationId].worldPosition - new Vector2(transform.localPosition.x, transform.localPosition.y);
                float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - 90;
                spotLight.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

                //Now let's change the sprite.

                changeSprite(dir, currentAnimationIndex);

            }
            catch (System.Exception e)
            {
                Debug.Log("We've got an error!");
                Debug.Log("destinationId = "+ destinationId);
                Debug.Log("pathToDestinationId = " + pathToDestinationId);
                Debug.Log("pathToDestination max number is: " + pathToDestination.Count);
                Debug.Log("pathToDestination smaller max number is: " + pathToDestination[destinationId].Count);
                Debug.Log("Testing to if it works:" + pathToDestination[destinationId][pathToDestinationId].gridX);
                Debug.Log(e.ToString());
            }
        }
        //If we're shooting the player.
        else
        {
            directionUpdateTime+= Time.deltaTime;
            shootUpdateTime += Time.deltaTime;
            shootingTimeoutTime += Time.deltaTime;
            if (directionUpdateTime > 0.3f)
            {
                //Let's update the spotlight rotation to rotate towards the player
                Vector2 dir = playerCharacter.transform.position - transform.position;
                float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - 90;
                spotLight.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
                changeSprite(dir, currentAnimationIndex);

                //The shooting direction is important as well.
                shootingDirection = playerCharacter.transform.position - transform.position;
                directionUpdateTime = 0;
            }
            if (shootUpdateTime > shootSpeed)
            {
                //We use this raycast to make sure that the enemy can still see the player. To make it a little easier.
                RaycastHit2D toPlayerHit = Physics2D.Linecast(transform.position, playerCharacter.transform.position);
                Debug.Log("raycast name: " + toPlayerHit.transform.name);
                if (toPlayerHit.transform == playerCharacter.transform)
                {
                    EnemyShootingPlayer();
                    shootUpdateTime = 0;
                    shootSpeed = Random.Range(0.2f, 0.6f);
                    shootingTimeoutTime = 0;
                }
                else if (shootingTimeoutTime > 2.0f)
                {
                    foundPlayer = false;
                }
                
                
            }
            

        }
        //Astar.transform.position += Vector3.left*0.05f;
        
        Vector2 rayDirection = pathToDestination[destinationId][pathToDestinationId].worldPosition - new Vector2(transform.localPosition.x, transform.localPosition.y);
        Debug.DrawRay(transform.position, rayDirection, Color.red, 0.1f);
        RaycastHit2D hit = Physics2D.Raycast (transform.position, rayDirection, 5.0f);
        if (hit.collider != null) 
        {
			//other.gameObject.CompareTag("Whatever");
			if( (hit.transform.CompareTag( "Player"))&&(!isEnemyShooting)) {
				shootingDirection = playerCharacter.transform.position - transform.position;
				isEnemyShooting=true;
                foundPlayer = true;
         // enemy can see the player!
        } else {
         // there is something obstructing the view
        }
        }
    }
	void EnemyShootingPlayer(){
		//Vector2 shootingDirection = playerCharacter.transform.position - transform.position;

		GameObject bullet = Instantiate (enemyBullet, transform.position, Quaternion.identity) as GameObject;//this gotta be the direction the player is facing
		bullet.GetComponent<Bullet>().isEnemyBullet=true;

		bullet.GetComponent<Rigidbody2D>().velocity = bullet.GetComponent<Bullet>().velocity*shootingDirection;//player -enemy transform position

		bullet.transform.parent = bulletParent.transform;

	
	}

    void changeSprite(Vector3 dir, int index)
    {
        if (dir.x < -0.1f)
        {
            if (dir.y > 0.1f)
            {
                //Upper left
                if (spriteRenderer.sprite != enemySprites[12+index])
                {
                    spriteRenderer.sprite = enemySprites[12+index];
                }
            }
            else if (dir.y < -0.1f)
            {
                //Lower left
                if (spriteRenderer.sprite != enemySprites[18+index])
                {
                    spriteRenderer.sprite = enemySprites[18+index];
                }
            }
            else
            {
                //Left
                if (spriteRenderer.sprite != enemySprites[6+index])
                {
                    spriteRenderer.sprite = enemySprites[6+index];
                }
            }

        }
        else if (dir.x > 0.1f)
        {

            if (dir.y > 0.1f)
            {
                //Upper right
                if (spriteRenderer.sprite != enemySprites[15+index])
                {
                    spriteRenderer.sprite = enemySprites[15+index];
                }
            }
            else if (dir.y < -0.1f)
            {
                //Lower right
                if (spriteRenderer.sprite != enemySprites[21+index])
                {
                    spriteRenderer.sprite = enemySprites[21+index];
                }
            }
            else
            {
                //Right
                if (spriteRenderer.sprite != enemySprites[9+index])
                {
                    spriteRenderer.sprite = enemySprites[9+index];
                }
            }
        }
        else
        {

            if (dir.y >= 0)
            {
                //Going up
                if (spriteRenderer.sprite != enemySprites[0+index])
                {
                    spriteRenderer.sprite = enemySprites[0+index];
                }
            }
            else
            {
                //Going down
                if (spriteRenderer.sprite != enemySprites[3+index])
                {
                    spriteRenderer.sprite = enemySprites[3+index];
                }
            }

        }
    }

}
