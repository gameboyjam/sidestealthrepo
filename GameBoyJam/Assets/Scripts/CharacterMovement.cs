﻿using UnityEngine;
using System.Collections;

public class CharacterMovement : MonoBehaviour {

	public Shooting characterShooting;
	public float speed = 5,tempSpeed;
	private Animator anim;
	private float movex,movey;
	private float xmin, xmax, ymin, ymax;
	private Rigidbody2D rb2d;

    private Vector2 lookAt;
	//private string[] movements = new string[]{ "up", "down", "right", "left" };
	// Use this for initialization
	void Start ()
    {
		anim = GetComponent<Animator> ();
		characterShooting = GetComponent<Shooting> ();
        rb2d = gameObject.GetComponent<Rigidbody2D>();
        lookAt = new Vector2(1, 0);
	}
	// Update is called once per frame
	void Update () {

		if (!Input.GetKey (KeyCode.I))
        {
			MovementAnimation ();
		}
		if (Input.GetKey (KeyCode.I)) {
			
			HiddingAnimation ();
		}
		ChangeSpeed ();
        movex = Input.GetAxis ("Horizontal");
		movey = Input.GetAxis ("Vertical");
    }
	void MovementAnimation ()
	{

		anim.SetBool ("isCrouching", false);

		if (Input.GetKey ("left") || Input.GetKey(KeyCode.A))
        {
            lookAt = new Vector2(-1, 0);
			WalkAnimation (-1, 0, true);
		}
		if (Input.GetKey ("right") || Input.GetKey(KeyCode.D))
        {
            lookAt = new Vector2(1, 0);
            WalkAnimation (1, 0, true);
		}
		if (Input.GetKey ("up") || Input.GetKey(KeyCode.W))
        {
            lookAt = new Vector2(0, 1);
            WalkAnimation (0, 1, true);
		}
        if (Input.GetKey ("down") || Input.GetKey(KeyCode.S))
        {
            lookAt = new Vector2(0, -1);
            WalkAnimation (0, -1, true);
		}

		/*if ((movex == 0) && (movey == 0))
        {
			_rigidbody2D.velocity = Vector2.zero;
		}	*/
	}

	//let see what happens with the velocities
	void HiddingAnimation ()
	{

		anim.SetBool ("isWalking", false);

		if (Input.GetKey ("left") || Input.GetKey(KeyCode.A))
        {
            lookAt = new Vector2(-1, 0);
            CrouchAnimation (-1, 0, true);
		}
		if (Input.GetKey ("right") || Input.GetKey(KeyCode.D))
        {
            lookAt = new Vector2(1, 0);
            CrouchAnimation (1, 0, true);
		}
		if (Input.GetKey ("up") || Input.GetKey(KeyCode.W))
        {
            lookAt = new Vector2(0, 1);
            CrouchAnimation (0, 1, true);
		}
        if (Input.GetKey ("down") || Input.GetKey(KeyCode.S))
        {
            lookAt = new Vector2(0, 1);
            CrouchAnimation (0, -1, true);
		}

		/*if ((movex == 0) && (movey == 0))
        {
			_rigidbody2D.velocity = Vector2.zero;
		}*/
	}

	public void StopCharacter()
    {
        //For some reason if this code is enabled you can't move in WebGL.
        #if !UNITY_WEBGL
		speed = 0;
        #endif
        tempSpeed = speed;

    }
	public void ResumeMovingCharacter()
    {
        #if !UNITY_WEBGL
        speed = tempSpeed;
        #endif
    }

    void WalkAnimation(float x,float y,bool isWalking)
    {
		anim.SetFloat ("velX", x);
		anim.SetFloat ("velY", y);
		anim.SetBool ("isWalking", isWalking);
	}

	void CrouchAnimation(float x,float y,bool isCrouching)
    {
		anim.SetFloat ("crouchX", x);
		anim.SetFloat ("crouchY", y);
		anim.SetBool ("isCrouching", isCrouching);
	}

	void FixedUpdate()
	{
		rb2d.velocity = new Vector2 (movex * speed, movey * speed);
		if ((movex == 0) && (movey == 0))
        {
			rb2d.velocity = Vector2.zero; 
		}
	}

	void ChangeSpeed()
    {
		if (Input.GetKeyDown (KeyCode.I)) {
			//the character sneaks here
			speed=speed/3;
		}
		if (Input.GetKeyUp (KeyCode.I)) {
			//the character changes his speed to normal here
			speed=speed*3;
		}
	}

    public Vector2 lookingDirection()
    {
        return lookAt;
    }
}
