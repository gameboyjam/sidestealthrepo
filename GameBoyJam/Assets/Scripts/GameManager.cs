﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

    public GameObject floor;
    public GameObject creator;
    public float timeToStop;
    public float textureChangeInMeters;
    private float m_lastTxtMeters;


    // Use this for initialization
    void Start ()
    {
        SpotLight.onSpotLightReached += KillPlayer;
        FloorScrolling.onMeters += MetersPassed;

	}
	
  
	// Update is called once per frame
	void OnDestroy ()
    {
        SpotLight.onSpotLightReached -= KillPlayer;
        FloorScrolling.onMeters -= MetersPassed;
	}

    public void StopMovement()
    {
        floor.GetComponent<FloorScrolling>().SetMove(false);
        creator.GetComponent<ObstacleCreator>().stop();
        ObstacleMovement[] obstacles = FindObjectsOfType<ObstacleMovement>();
        foreach( ObstacleMovement o in obstacles)
        {
            o.SetMove(false);
        }
        //Invoke("StartMovement", timeToStop);
        InvokeRepeating("CheckNoEnemies", 1.0f, 1.0f);
    }

    void KillPlayer()
    {
        FindObjectOfType<PlayearHealth>().KillPlayer();
    }

    public void StartMovement()
    {
        floor.GetComponent<FloorScrolling>().SetMove(true);
        creator.GetComponent<ObstacleCreator>().start();
        ObstacleMovement[] obstacles = FindObjectsOfType<ObstacleMovement>();
        foreach (ObstacleMovement o in obstacles)
        {
            o.SetMove(true);
        }
    }

    void CheckNoEnemies()
    {
        EnemyScript[] enemies = FindObjectsOfType<EnemyScript>();
        if(enemies.Length == 0)
        {
            CancelInvoke();
            StartMovement();
        }
    }
    void MetersPassed(float m_meters)
    {
        float cMeters = m_meters - m_lastTxtMeters;

        if(cMeters > textureChangeInMeters)
        {
            FindObjectOfType<FloorScrolling>().swapTextures();
            m_lastTxtMeters = m_meters;
        }
    }
}
