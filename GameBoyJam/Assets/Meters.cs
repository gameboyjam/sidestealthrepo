﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Meters : MonoBehaviour {
	public Text metersText;
	private FloorScrolling floor;
	//private float metersAdvanced;
	// Use this for initialization
	void Start () {
		floor = FindObjectOfType<FloorScrolling> ();

	}

	// Update is called once per frame
	void Update () {
		metersText.text =Mathf.RoundToInt( floor.getMeters ()).ToString()+"m";
	}
}
