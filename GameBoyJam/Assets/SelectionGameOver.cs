﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class SelectionGameOver : MonoBehaviour {
	private List<string> selectionStates = new List<string> {"left","right"};
	public Transform[] positions;
	public string actualSelection="left";
	private int actualSelectionNumber;
	private LevelManager levelManager;
	public AudioClip switchSound;
	// Use this for initialization
	void Start () {
		levelManager = FindObjectOfType<LevelManager> ();
		gameObject.AddComponent<AudioSource>();
		switchSound = Resources.Load ("switchSound") as AudioClip;
	}

	// Update is called once per frame
	void Update () {
		actualSelection = selectionStates [actualSelectionNumber];

		if (actualSelection == "left") {//this is to play
			transform.position = positions [0].position;
			if (Input.GetKey (KeyCode.J)) {
				levelManager.LoadLevel ("Game");
			}
		}
		if (actualSelection == "right") {//menu
			transform.position = positions [1].position;
			if (Input.GetKey (KeyCode.J)) {
				levelManager.LoadLevel ("MainMenu");
			}
		}



		if (Input.anyKey) {
			SwitchSelections ();

		}

	}

	void SwitchSelections(){
		

		if (Input.GetKeyDown (KeyCode.LeftArrow)) {
			GetComponent<AudioSource> ().PlayOneShot (switchSound);
			actualSelectionNumber=(actualSelectionNumber+1)%2;
			print (actualSelectionNumber);

		}
		if (Input.GetKeyDown (KeyCode.RightArrow)) {
			GetComponent<AudioSource> ().PlayOneShot (switchSound);
			actualSelectionNumber=(actualSelectionNumber-1)%2;
			if (actualSelectionNumber == -1) {
				actualSelectionNumber = 1;
			}
			print (actualSelectionNumber);

		}


	}

}
