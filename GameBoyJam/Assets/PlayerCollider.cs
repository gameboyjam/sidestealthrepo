﻿using UnityEngine;
using System.Collections;

public class PlayerCollider : MonoBehaviour {
	private PlayearHealth playerHealth;

	public Sprite playerDeath;
	// Use this for initialization
	void Start () {
		playerHealth = FindObjectOfType<PlayearHealth> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/*void OnTriggerEnter2d(Collider2D other){
		if (other.GetComponent<Bullet> ()) {
			if (other.GetComponent<Bullet> ().isEnemyBullet) {
				Debug.Log ("playercolliderhitbyEnemyBullet");
			
			}
		}

	}*/

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.GetComponent<Bullet> ()) {
			if (other.GetComponent<Bullet> ().isEnemyBullet) {
				playerHealth.health--;
				print ("playerGettingShotAt");
				other.gameObject.GetComponent<SpriteRenderer>().enabled=false;
			}
		}



        if(!other.CompareTag("SpotLight"))
        {
            if (playerHealth.health <= 0)
            {
                print("playerGotKilled!");
				playerHealth.KillPlayer ();
               
            }
        }


	}
}
