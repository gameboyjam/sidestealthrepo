﻿using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour {
	public static bool paused = false;
	public GameObject pauseMenu;
	private LevelManager levelManager;
	// Use this for initialization
	void Start () {
		levelManager = FindObjectOfType<LevelManager> ();

	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			paused = !paused;
			pauseMenu.SetActive (!pauseMenu.activeSelf);

		}

		if (paused) {
			Time.timeScale = 0;
			FindObjectOfType<GameManager> ().StopMovement ();



		}
		if (!paused) {
			pauseMenu.SetActive (false);
			Time.timeScale = 1;


		}




	}

	public void Resume(){
		FindObjectOfType<GameManager> ().StartMovement ();
		paused = false;

	}
}
